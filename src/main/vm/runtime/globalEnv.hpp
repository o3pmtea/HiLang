#ifndef GLOBAL_ENV_HPP
#define GLOBAL_ENV_HPP

#include "object/hiString.hpp"

class GlobalEnv {
private:
    static const int    PATH_MAX_LEN = 256;

    HiString*           _exec_path; // executable path
    HiString*           _exec_name; // executable name
    void                get_executable_info();

    static GlobalEnv*   _instance;
    GlobalEnv();

public:
    static GlobalEnv*   get_instance();

    HiString*           exec_path() {return _exec_path;}
    HiString*           exec_name() {return _exec_name;}

    void                oops_do(OopClosure* f);
};

#endif