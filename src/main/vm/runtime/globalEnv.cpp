#include "runtime/globalEnv.hpp"
#include "memory/oopClosure.hpp"

GlobalEnv* GlobalEnv::_instance = NULL;

GlobalEnv* GlobalEnv::get_instance() {
    if (_instance == NULL) {
        _instance = new GlobalEnv();
    }
    return _instance;
}

GlobalEnv::GlobalEnv() {
    get_executable_info();
}

void GlobalEnv::oops_do(OopClosure* f) {
    f->do_oop((HiObject**)&_exec_path);
    f->do_oop((HiObject**)&_exec_name);
}
