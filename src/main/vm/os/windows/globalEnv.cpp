#include "runtime/globalEnv.hpp"
#include <string.h>
#include <windows.h>

void GlobalEnv::get_executable_info() {
    char buffer[PATH_MAX_LEN + 1] = {0};
    int len = GetModuleFileNameA(NULL, buffer, PATH_MAX_LEN);
    if ((len <= 0) || (GetLastError() != ERROR_SUCCESS)) {
        fprintf(stderr, "Error : failed to get exe path.");
        assert(false);
    }
    char* lastSep = strrchr(buffer, '\\');
    if (lastSep == NULL) {
        fprintf(stderr, "Error : get executable path failed.");
        assert(false);
    }
    _exec_name = new HiString(lastSep + 1);
    *lastSep = '\0';
    _exec_path = new HiString(buffer);
}
