#include "runtime/globalEnv.hpp"
#include <unistd.h>
#include <string.h>
#include <libproc.h>

void GlobalEnv::get_executable_info() {
    char buffer[PROC_PIDPATHINFO_MAXSIZE] = {0};
    pid_t pid = getpid();
    int len = proc_pidpath(pid, buffer, sizeof(buffer));
    if (len <= 0) {
        fprintf(stderr, "Error : failed to get exe path.");
        assert(false);
    }
    char* lastSep = strrchr(buffer, '/');
    if (lastSep == NULL) {
        printf("Error : get executable path failed.");
        assert(false);
    }
    _exec_name = new HiString(lastSep + 1);
    *lastSep = '\0';
    _exec_path = new HiString(buffer);
}
