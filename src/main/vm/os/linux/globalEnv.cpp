#include "runtime/globalEnv.hpp"
#include <unistd.h>
#include <string.h>

void GlobalEnv::get_executable_info() {
    char buffer[PATH_MAX_LEN + 1] = {0};
    int len = readlink("/proc/self/exe", buffer, PATH_MAX_LEN);
    if (len < 0) {
        printf("Error : readlink(\"/proc/self/exe\") failed.");
        assert(false);
    }
    buffer[len] = '\0';
    char* lastSep = strrchr(buffer, '/');
    if (lastSep == NULL) {
        printf("Error : get executable path failed.");
        assert(false);
    }
    _exec_name = new HiString(lastSep + 1);
    *lastSep = '\0';
    _exec_path = new HiString(buffer);
}
