def dumps(co, fc):
    data = bytearray(4)
    data.extend(bytearray(4))
    dump_codeobject(co, data)
    fc.write(data)

def dump_codeobject(co, data):
    data.append(ord('c'))
    data.extend(co.co_argcount.to_bytes(4, "little"))
    data.extend(co.co_nlocals.to_bytes(4, "little"))
    data.extend(co.co_stacksize.to_bytes(4, "little"))
    data.extend(co.co_flags.to_bytes(4, "little"))

    dump_bytes(co.co_code, data)
    dump_tuple(co.co_consts, data)
    dump_tuple(co.co_names, data)
    dump_tuple(co.co_varnames, data)
    dump_tuple(co.co_freevars, data)
    dump_tuple(co.co_cellvars, data)
    dump_string(co.co_filename, data)
    dump_string(co.co_name, data)
    data.extend(co.co_firstlineno.to_bytes(4, "little"))
    dump_bytes(co.co_lnotab, data)

def dump_string(s, data):
    data.append(ord('s'))
    data.extend(len(s).to_bytes(4, "little"))
    data.extend(s.encode("ascii"))
    
def dump_bytes(s, data):
    data.append(ord('s'))
    data.extend(len(s).to_bytes(4, "little"))
    data.extend(s)
    
def dump_tuple(t, data):
    data.append(ord('('))
    data.extend(len(t).to_bytes(4, "little"))

    for x in t:
        dump_element(x, data)

def dump_element(x, data):
    if x is None:
        data.append(ord('N'))
    elif type(x).__name__ == "code":
        dump_codeobject(x, data)
    elif isinstance(x, int):
        data.append(ord('i'))
        data.extend(x.to_bytes(4, "little"))
    elif isinstance(x, str):
        dump_string(x, data)
    elif isinstance(x, bytes):
        dump_bytes(x, data)
    elif isinstance(x, dict):
        dump_dict(x, data)
    elif isinstance(x, list):
        dump_list(x, data)

def dump_dict(x, data):
    data.append(ord('d'))
    data.extend(len(x).to_bytes(4, "little"))

    for k, v in x.items():
        dump_element(k)
        dump_element(v)
