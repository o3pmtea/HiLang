#-*- coding: UTF-8 -*-
#
# -----------------------------------------------------------------------------
# parser for HiLang
# author : hinus@163.com
# date : 2020-06-30
# -----------------------------------------------------------------------------


import ply.lex as lex

reserved = {
	'class' : 'TAG_CLASS',
	'func' : 'TAG_DEF',
	'from' : 'TAG_FROM',
	'as' : 'TAG_AS',
	'global' : 'TAG_GLOBAL',
	
	'if' : 'TAG_IF',
	'else' : 'TAG_ELSE',
	'elif' : 'TAG_ELIF',
	'for' : 'TAG_FOR',
	'while' : 'TAG_WHILE',
	'try' : 'TAG_TRY',
	'except' : 'TAG_EXCEPT',
	'finally' : 'TAG_FINALLY',
	
	'or' : 'TAG_OR',
	'and' : 'TAG_AND',
	'not' : 'TAG_NOT',
	'in' : 'TAG_IN',
	'is' : 'TAG_IS',
	
	'pass' : 'TAG_PASS',
	'del' : 'TAG_DEL',
	'break' : 'TAG_BREAK',
	'continue' : 'TAG_CONTINUE',
	'return' : 'TAG_RETURN',
	'raise' : 'TAG_RAISE',
	'exec' : 'TAG_EXEC',
	'yield' : 'TAG_YIELD',
	'import' : 'TAG_IMPORT',
}

literals = ['+', '-', '*', '/', ':', '(', ')', '{', '}',
    '[', ']', '<', '>', '=', ',', '.', '%', '|', '&', '^', '?']

tokens = (
    'NEWLINE',
    'ENDMARKER',
    'STRING',
    
    'TAG_CLASS',
    'TAG_DEF',
    'TAG_LAMBDA',
    'TAG_FROM',
    'TAG_AS',
    'TAG_GLOBAL',
    
    'TAG_IF',
    'TAG_ELSE',
    'TAG_ELIF',
    'TAG_FOR',
    'TAG_WHILE',
    'TAG_TRY',
    'TAG_EXCEPT',
    'TAG_FINALLY',
    
    'TAG_PASS',
    'TAG_DEL',
    'TAG_BREAK',
    'TAG_CONTINUE',
    'TAG_RETURN',
    'TAG_RAISE',
    'TAG_EXEC',
    'TAG_YIELD',
    'TAG_IMPORT',
    
    'TAG_OR',
    'TAG_AND',
    'TAG_NOT',
    'TAG_IN',
    'TAG_IS',
    
    'OP_LE',
    'OP_GE',
    'OP_EQ',
    'OP_NE',
    'OP_NNE',
    
    'OP_LEFT_SHIFT',
    'OP_RIGHT_SHIFT',
    'OP_EXACT_DIVISION',
    'OP_POWER',

    'ADD_ASN',
    'SUB_ASN',
    'MUL_ASN',
    'DIV_ASN',
    'MOD_ASN',
    'AND_ASN',
    'OR_ASN',
    'XOR_ASN',
    'LSHIFT_ASN',
    'RSHIFT_ASN',
    'POW_ASN',
    'FDIV_ASN',
    
    'NAME',
    'NUMBER',
)

t_ignore = " \t"

def t_NEWLINE(t):
    r';'
    return t

t_TAG_LAMBDA = r'\\'

t_OP_EQ = r'=='
t_OP_LE = r'<='
t_OP_GE = r'>='
t_OP_NE = r'!='

t_ADD_ASN = r'\+='
t_SUB_ASN = r'-='
t_MUL_ASN = r'\*='
t_DIV_ASN = r'\/='
t_MOD_ASN = r'\%='
t_AND_ASN = r'&='
t_OR_ASN  = r'\|='
t_XOR_ASN = r'^='
t_LSHIFT_ASN = r'<<='
t_RSHIFT_ASN = r'>>='
t_POW_ASN = r'\*\*='
t_FDIV_ASN   = r'//='

t_OP_LEFT_SHIFT = r'<<'
t_OP_RIGHT_SHIFT = r'>>'
t_OP_EXACT_DIVISION = r'//'
t_OP_POWER = r'\*\*'
    
t_STRING = r'\".*?\"'


def t_NUMBER(t):
    r'\d+'
    return t
    
def t_NAME(t):
    r'[a-zA-Z_][a-zA-Z0-9_]*'
    t.type = reserved.get(t.value, "NAME")
    return t
    
def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")
    
def t_eof(t):
    if lex.eof:
        return None
    t = lex.LexToken()
    t.type = 'ENDMARKER'
    t.value = 'EOF'
    t.lineno = -1
    t.lexpos = 0
    lex.eof = True
    return t
    
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

lex.lex(debug = False)
lex.eof = False
