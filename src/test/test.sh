#!/bin/bash
ROOT_PATH=$(readlink -f $(dirname ${0}))
PARSER=${ROOT_PATH}/../main/parser/parser.py
BUILD_PATH=${ROOT_PATH}/../../build
HILANG_PATH=${ROOT_PATH}/hilang

function compile_and_move() {
    python3 ${PARSER} ${1}
    mv ${1}b ${BUILD_PATH}
}

file_list=(
    ${HILANG_PATH}/datas.hi
    ${HILANG_PATH}/expr.hi
    ${HILANG_PATH}/function.hi
    ${HILANG_PATH}/high_order.hi
    ${HILANG_PATH}/make_func.hi
    ${HILANG_PATH}/trect.hi
)

for i in ${file_list[*]}; do
    compile_and_move $i
done
