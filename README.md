# HiLang

#### 介绍
创建一门新的语言，用于图形渲染，物理引擎模拟，科学计算。

#### 软件架构
本项目包含HiLang的编译器，用于将HiLang源文件翻译成字节码文件；
以及一个虚拟机，用于执行字节码文件。

#### 构建环境要求
##### 通用
cmake, python2, python3

##### MacOS
coreutil

##### Windows
msys2, mingw-w64

#### 安装教程

在根目录下执行
```
./build.sh
```
构建脚本将会在根目录下新建 build 目录，虚拟机和编译器都会安装在这个目录下。
如果将虚拟机(railgun)所在的目录添加至系统PATH，则可以在任意文件夹下启动虚拟机。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


