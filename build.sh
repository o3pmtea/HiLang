#!/bin/bash

function check_env() {
    if [[ $ENV_NAME == Linux* ]]; then
        ENV_NAME="Linux"
        DLL_POSTFIX="so"
        READLINK="readlink"
        MAKE="make"
        PYTHON2="python"
        PYTHON3="python3"
    elif [[ $ENV_NAME == MINGW* ]]; then
        ENV_NAME="MINGW"
        DLL_POSTFIX="dll"
        READLINK="readlink"
        MAKE="mingw32-make"
        PYTHON2="python2"
        PYTHON3="python"
    elif [[ $ENV_NAME == Darwin* ]]; then
        ENV_NAME="Darwin"
        DLL_POSTFIX="dylib"
        READLINK="greadlink"
        MAKE="make"
        PYTHON2="python"
        PYTHON3="python3"
    else
        ENV_NAME="UNKNOWN"
    fi
}

# build railgun
function build_vm() {
    if [ ! -d ${VM_BUILD_PATH} ]; then
        mkdir ${VM_BUILD_PATH}
    fi
    cd ${VM_BUILD_PATH}
    if [[ $ENV_NAME == "Linux" ]] || [[ $ENV_NAME == "Darwin" ]] ; then
        cmake ${VM_SRC_PATH} -G "Unix Makefiles"
    elif [[ $ENV_NAME == "MINGW" ]]; then
        cmake ${VM_SRC_PATH} -G "MinGW Makefiles"
    fi
    $MAKE all
    post_make
}

# move libmath and compile builtin file
function post_make() {
    cd ${VM_BUILD_PATH}
    mkdir -p lib
    cp libmath.$DLL_POSTFIX lib/
    cp ${VM_SRC_PATH}/lib/*.py lib/
    $PYTHON2 -m compileall lib/
}

function create_hic() {
    echo '
    #!/bin/bash
    if [ -z "$1" ];then
        echo "Please input .hi file path"
        exit 1
    fi

    # self path
    ROOT_PATH=$('$READLINK' -f $(dirname ${0}))
    PARSER_PATH=${ROOT_PATH}/../src/main/parser/parser.py

    for arg in "$@"
    do
        if [[ ${arg} = *.hi ]]; then # file ends with .hi
            '$PYTHON3' ${PARSER_PATH} ${arg}
        fi
    done' > ${VM_BUILD_PATH}/hic
    chmod +x ${VM_BUILD_PATH}/hic
}

ENV_NAME=$(uname)
DLL_POSTFIX=
READLINK=
MAKE=
PYTHON2=
PYTHON3=

check_env
echo "ENV_NAME=${ENV_NAME}"

ROOT_PATH=$($READLINK -f $(dirname ${0}))
VM_SRC_PATH=${ROOT_PATH}/src/main/vm
VM_BUILD_PATH=${ROOT_PATH}/build

build_vm
create_hic
